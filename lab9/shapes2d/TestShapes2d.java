package shapes2d;

public class TestShapes2d {

    public static void main(String[] args){
        Circle c = new Circle(5);
        c.area();

        System.out.println(c.toString());

        Square s = new Square(4);

        System.out.println(s.toString());
    }
}
